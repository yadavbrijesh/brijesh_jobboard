function JobBrief(props) {
  const { name, location, salary, description, logo } = props.job;
  const { city, country } = location;

  return (
    <div>
      <h4>{name}</h4>
      <h5>{city}, {country}</h5>
      <img src={logo} className="img img-thumbnail" />
      <p>{description}</p>
      <div>Salary: {salary / 1000}K</div>
      <hr />
    </div>
  );
}

export default JobBrief;