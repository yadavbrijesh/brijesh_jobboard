import Jobs from "./jobs.json";
import JobBriefList from "./JobBriefList";

function App() {
  return (
    <div className="container">
      <JobBriefList jobs={Jobs} />
    </div>
  );
}

export default App;
