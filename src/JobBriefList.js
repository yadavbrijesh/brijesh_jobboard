import JobBrief from "./JobBrief";

function JobBriefList({ jobs }) {
  const [job0, job1, job2, job3, job4] = jobs;
  return (
    <div style={{ maxWidth: "300px", alignItems: "center" }}>
      <JobBrief job={job0} />
      <JobBrief job={job1} />
      <JobBrief job={job2} />
      <JobBrief job={job3} />
      <JobBrief job={job4} />
    </div>
  );
};

export default JobBriefList;